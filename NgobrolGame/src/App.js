import { View, Text, ScrollView, navigation, Button, StyleSheet, Alert, StatusBar} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Router from './Router';
import React from 'react';


function App() {
  return (
    
      <NavigationContainer>
        <Router/>
      </NavigationContainer>
    
  );
}

export default App;
