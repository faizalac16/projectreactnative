import React, {useEffect} from 'react';
import {View,StyleSheet, Image} from 'react-native';

const Splash = ({navigation})=>{
  useEffect(()=>{
    setTimeout(() => {
        navigation.replace('Login');
    }, 2000);
  })
  return(
    <View style={styles.container}>
      <Image source={require('../Images/1.png')}
      style={styles.images}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  images:{
    width:315,
    height:315
  }


})


export default Splash;