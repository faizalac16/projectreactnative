import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TextInput, 
  Button, 
  TouchableOpacity, 
  Alert,
  AsyncStorage} from 'react-native';

const userInfo = {username:'admin', password:'pass1234'}

class Login extends React.Component{
 
    static navigationOption ={
      header:null
    }
  
    constructor(props){
      super(props);
      this.state={
        username: '',
        password:''
      }
    }

    render(){
      return(
      <View style={styles.splasharea}>
        <Image source={require('../Images/1.png')} style={styles.images}/>
        
        <Text style={styles.text}>LOGIN</Text>
        
        <View style={styles.inputarea}>
            <TextInput 
            style={styles.Input}
            placeholder={'Username'} 
            placeholderTextColor="#DE5FB2" 
            onChangeText={(username)=>this.setState({username})}
            value={this.state.username}
            autoCapitalize="none"
            />

            <TextInput 
            style={styles.Input}
            placeholder={'Username'} 
            placeholderTextColor="#DE5FB2"
            secureTextEntry={true} 
            onChangeText={(password)=>this.setState({password})}
            value={this.state.password}
            autoCapitalize="none"
            />
        </View>

        <TouchableOpacity activeOpacity={0.95} 
        style={styles.button} 
        onPress={this._login}>
            <Text style={styles.textBtn} >LOGIN</Text>
        </TouchableOpacity>

      </View>
      )
    }

    _login = async() =>{
      if(userInfo.username == this.state.username && userInfo.password == this.state.password){
        this.props.navigation.replace('Home');
      }else{
        alert('User Did not Found');
      }
    }

   
}




const styles = StyleSheet.create({
  splasharea:{
    flex:1,
    alignItems:'center'
  },
  images:{
      width:315,
      height:315
  },
  text:{
      fontSize:40,
      color:"#DE5FB2",
      fontWeight:'bold'
  },
  Input:{
      borderWidth:2,
      width:273,
      marginTop:20,
      marginBottom:10,
      height:61,
      padding:10,
      borderColor:"#DE5FB2"
  },
  inputarea:{
      marginTop:25,
      marginBottom:20
  },
  button: {
    width: 100,
    height: 100,
    padding: 20,
    fontSize:100
  },
    button: {
        height: 51, 
        backgroundColor: '#DE5FB2',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        elevation:3,
        width:151
    },
    textBtn:{
        fontSize: 24,
        fontWeight: 'bold',
        color:'#FFFFFF'
    }
})


export default Login;