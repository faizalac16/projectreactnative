import React, { useDebugValue, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {Splash, Login, Home, News} from '../Pages';


const Stack = createStackNavigator();



const Router = () =>{
   return(
    <Stack.Navigator headerMode="none">
        <Stack.Screen  name="Splash" component={Splash}/>
        <Stack.Screen  name="Login" component={Login}/>
        <Stack.Screen  name="Home" component={Home}/>
        <Stack.Screen  name="News" component={News}/>
    </Stack.Navigator>
   );
}



export default Router;